from rest_framework import serializers
from .models import Transaction, Organization


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = ('id','payment_method', 'payment_reference', 'amount', 'mobile_number', 'first_name', 'middle_name', 'last_name', 'account_number','invoice_number', 'transaction_date', 'organization')

    def create(self, validated_data):
        """
        Create and return a new `Order` instance, given the validated data.
        """
        return Transaction.objects.create(**validated_data)

'''
class TransactionDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = ('id', 'order_status', 'order_udatetime')

    def update(self, instance, validated_data):
        """
        Update and return an existing `Order` instance, given the validated data.
        """
        #instance.organization = validated_data.get('organization', instance.organization)
        #instance.mobile_number = validated_data.get('mobile_number', instance.mobile_number)
        #instance.document_type = validated_data.get('document_type', instance.document_type)
        #instance.document_number =  validated_data.get('document_number', instance.document_number)
        instance.order_udatetime = validated_data.get('order_udatetime', instance.order_udatetime)
        instance.order_status = validated_data.get('order_status', instance.order_status)
        instance.save()
        return instance'''

'''
class InformDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrganizationOrders001
        fields = ('id',
            'order_status',
            'order_udatetime',
            'existence_index',
            'defaulter_index',
            'overdue_index',
            'aggregate_index',
            'overall_score',
            'subscriber_firstname',
            'subscriber_lastname',
            'scoring_data_id',
            'algorithm_id')

    def update(self, instance, validated_data):
        """
        Update and return an existing `Order` instance, given the validated data.
        """
        instance.existence_index = validated_data.get('existence_index', instance.existence_index)
        instance.defaulter_index = validated_data.get('defaulter_index', instance.defaulter_index)
        instance.overdue_index = validated_data.get('overdue_index', instance.overdue_index)
        instance.aggregate_index = validated_data.get('aggregate_index', instance.aggregate_index)
        instance.overall_score = validated_data.get('overall_score', instance.overall_score)
        instance.subscriber_firstname =  validated_data.get('subscriber_firstname', instance.subscriber_firstname)
        instance.subscriber_lastname =  validated_data.get('subscriber_lastname', instance.subscriber_lastname)
        instance.scoring_data_id =  validated_data.get('scoring_data_id', instance.scoring_data_id)
        instance.algorithm_id =  validated_data.get('algorithm_id', instance.algorithm_id)
        instance.order_udatetime = validated_data.get('order_udatetime', instance.order_udatetime)
        instance.order_status = validated_data.get('order_status', instance.order_status)
        instance.save()
        return instance
'''
