# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests
import json
from django.http import JsonResponse
import datetime
from rest_framework.parsers import JSONParser
import csv



@shared_task
def sendTransactionTask(Id, MSISDN, InvoiceNumber, TransID, TransTime, TransAmount, BillRefNumber, FirstName, MiddleName, LastName):
	url = 'http://www.mocky.io/v2/5ac8ec76320000540012fce0'
	data = {
	'Id': Id,
	'MSISDN': MSISDN,
	'InvoiceNumber': InvoiceNumber,
	'TransID': TransID,
	'TransTime': TransTime,
	'BillRefNumber': BillRefNumber,
	'KYCInfoList':[
		{
			"KYCName":"FirstName",
	        "KYCValue":FirstName
		},
		{
			"KYCName":"MiddleName",
	        "KYCValue":MiddleName
		},
		{
			"KYCName":"FirstName",
	        "KYCValue":LastName
		}

	]
	}
	headers = {'Content-Type': 'application/json'}
	response = requests.post(url, data=json.dumps(data), headers=headers)
	#Add If Statements for Error Handling

	response_data = json.loads(response.text)

	return "OK"



