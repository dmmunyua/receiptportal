# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User #added line to support User
from django.utils import timezone
from registration.signals import user_registered #added line to support User

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from django.utils.deconstruct import deconstructible

from django.db.models.signals import post_save
from django.dispatch import receiver
from receiptportal.tasks import sendTransactionTask #, processFileTask
import uuid, os, time, random, string
from django.conf import settings
from time import strftime
from uuid import uuid4



# Create your models here.

class Document(models.Model):

	
	PROCESS_OPTIONS = (
		('Completed', 'C'),
		('Running', 'R'),
		('Cancelled', 'N'),
		('Queued', 'Q'),
		)
	description = models.CharField(max_length=255, blank=True, default='Batch-'+time.strftime('%Y%m%d%H%M%S'))
	document = models.FileField()
	uploaded_at = models.DateTimeField(auto_now_add=True)
	processed = models.CharField(max_length=10, choices=PROCESS_OPTIONS, default='Queued')
	download = models.TextField(blank=True)
	organization = models.ForeignKey("Organization", default=1)

	def __unicode__(self):
		return str(self.description)	

class Organization(models.Model):
	organization_name=models.CharField(max_length=200, unique=True)
	organization_type=models.CharField(max_length=50)
	organization_code=models.CharField(max_length=6, unique=True)
	organization_shortcode=models.PositiveIntegerField(unique=True, validators=[MaxValueValidator(999999)])
	organization_krapin=models.CharField(max_length=11)
	organization_contactname=models.CharField(max_length=200)
	organization_contactnumber=models.IntegerField()

	def __unicode__(self):
		return str(self.organization_name)

class OrganizationUserProfile(models.Model):
	user=models.OneToOneField(User, unique=True)
	organization=models.ForeignKey("Organization")
	role=models.CharField(max_length=50, blank=True, null=True)


	def __unicode__(self):
		return str(self.user)

class Transaction(models.Model):
	PAYMENT_METHOD_OPTIONS = (
		('BANK', 'bank'),
		('CASH', 'cash'),
		('MPESA', 'mpesa'),
		)
	id=models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	payment_method=models.CharField(max_length=5, choices=PAYMENT_METHOD_OPTIONS, default='cash')
	payment_reference=models.CharField(max_length=50, blank=False, unique=True)
	invoice_number=models.CharField(max_length=50, blank=True, null=True)
	amount=models.DecimalField(max_digits=9, decimal_places=2)
	mobile_number=models.BigIntegerField()
	first_name=models.CharField(max_length=200, null=False)
	middle_name=models.CharField(max_length=200, blank=True, null=True)
	last_name=models.CharField(max_length=200, null=False)
	account_number=models.CharField(max_length=200, blank=True, null=True)
	transaction_date=models.DateTimeField(auto_now_add=True)
	status=models.CharField(max_length=200, blank=True, null=True)
	organization = models.ForeignKey("Organization", default=1)




  	
@receiver(post_save, sender=Transaction)
def getTransactionData(sender, instance, created, **kwargs):
	if created:
		Id = instance.id
		MSISDN = instance.mobile_number
		InvoiceNumber = instance.invoice_number
		TransID = instance.payment_reference
		TransTime =  instance.transaction_date.strftime("%Y%m%d%H%M%S")
		TransAmount = instance.amount
		BillRefNumber = instance.account_number
		FirstName = instance.first_name
		MiddleName = instance.middle_name
		LastName = instance.last_name
		'''for requester in Organization.objects.filter(organization_name=instance.organization):
			BusinessShortCode = requester.organization_shortcode
		if documentType == 'NATIONAL ID':
			convertDocumentType = 'National Id Card'
		else:
			convertDocumentType = documentType'''

		return sendTransactionTask.delay(Id, MSISDN, InvoiceNumber, TransID, TransTime, TransAmount, BillRefNumber, FirstName, MiddleName, LastName)


'''
@receiver(post_save, sender=Document)
def getDocumentData(sender, instance, created, **kwargs):
	if created:
		filepath = os.path.join(settings.MEDIA_ROOT, str(instance.document))
		batch = instance.id
		for requester in Organization.objects.filter(organization_name=instance.organization):
			organization =  requester.id
		return processFileTask.delay(filepath, organization, batch)'''

