from django.conf.urls import url
from django.contrib.auth.decorators import login_required


from . import views
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^transactions/', views.transactions, name='transactions'),
    #url(r'^purchase/', views.purchase, name='purchase'),
    url(r'^detail/(?P<transaction_id>[0-9a-f-]+)$', views.detail, name='detail'),
    #url(r'^detail/report/(\d+)/', views.report, name='report'),
    url(r'^transaction/', login_required(views.Transaction.as_view()), name = 'transaction'),
    #url(r'^transactiondetail/(?P<pk>\d+)/$', views.TransactionDetail.as_view(), name = 'transactiondetail'),
    #url(r'^inform/', views.inform, name='inform'),
    #url(r'^informdetail/(?P<pk>\d+)/$', views.InformDetail.as_view(), name='informdetail'),
    #url(r'^balance/', views.balance, name='balance'),
    url(r'^upload/', views.upload, name='upload'),
    url(r'^bulk/', views.bulk, name='bulk'),
    #url(r'^export/csv/(?P<pk>\d+)/$', views.export, name='export'),
]
